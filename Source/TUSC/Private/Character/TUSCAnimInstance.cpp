// Fill out your copyright notice in the Description page of Project Settings.


#include "Character/TUSCAnimInstance.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Character/TUSCCharacter.h"

UTUSCAnimInstance::UTUSCAnimInstance()
{
	bIsTargeting = false;
	bIsFalling = false;
	bIsMoving = false;

	Speed = 0.f;
	Direction = 0.f;

	Pitch = 0.f;
	Yaw = 0.f;
}

void UTUSCAnimInstance::NativeInitializeAnimation()
{
	Super::NativeInitializeAnimation();

	MyCharacter = Cast<ATUSCCharacter>(TryGetPawnOwner());

	if (MyCharacter)
		MyMovementComp = MyCharacter->GetCharacterMovement();
}

void UTUSCAnimInstance::UpdateAnimationProperties(float DeltaTime)
{
	if (!MyCharacter)
	{
		MyCharacter = Cast<ATUSCCharacter>(TryGetPawnOwner());

		if (!MyCharacter) return;
	}

	//if(Owner->IsA<ANCharacter::StaticClass()))

	FVector Velocity = MyCharacter->GetVelocity();
	Velocity.Z = 0.f;  //Get lateral speed only, not falling or jumping velocity.
	Speed = Velocity.Size();
	bIsMoving = Speed > 0.f;
	Direction = CalculateDirection(Velocity, MyCharacter->GetActorRotation());

	FRotator RotationDelta = UKismetMathLibrary::NormalizedDeltaRotator(MyCharacter->GetBaseAimRotation(), MyCharacter->GetActorRotation());
	Pitch = RotationDelta.Pitch;
	Yaw = RotationDelta.Yaw;

	if (MyMovementComp)
	{
		bIsFalling = MyMovementComp->IsFalling();
		bIsAccelerating = (MyMovementComp->GetCurrentAcceleration().Size() > 0);
	}
}