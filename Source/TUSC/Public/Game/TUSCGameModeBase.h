// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TUSCGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class TUSC_API ATUSCGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
